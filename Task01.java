package pp;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Алексей on 10.03.2017.
 */
public class Task01 {

        public static void main(String[] args) {
            List<String> x = Stream.of("A", "B", "C", "D", "I").collect(Collectors.toList());
            List<String> y = Stream.of("BD", "A", "ABC", "B", "M", "D", "M","C", "DC", "D","DF", "I").collect(Collectors.toList());
            System.out.println(check(x,y));
        }

        private static boolean check(List<String> x, List<String> y){
            System.out.println("x: " + x.toString());
            System.out.println("y: " + y.toString());
            outer:
            for (int i=0; i<x.size(); i++){
                String xString = x.get(i);
                for(int j = 0; j < y.size(); j++){
                    String yString = y.get(j);
                    if(!yString.equals(xString)){
                        y.remove(j--);
                    } else if(yString.equals(xString) && i == x.size() - 1){
                        return true;
                    } else {
                        continue outer;
                    }
                }
            }
            return false;
        }
    }