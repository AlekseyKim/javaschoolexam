package pp;

import javafx.util.Pair;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Алексей on 10.03.2017.
 */
public class Task03 {

    private static final String SYMBOL_PATTERN = "^[0-9]$";
    private static final String NUMBER_PATTERN = "^[0-9]+$";
    private static final String DOUBLE_NUMBER_PATTERN = "^[0-9]+\\.[0-9]+$";
    private static final String DOT = "^\\.$";
    private static OPNData data = new OPNData();

    public static void main(String[] args) {
        System.out.println("Result: " + calculate("243-12.2"));
    }

    private static String calculate(String evaluate){
        try {
            transferToOPN(evaluate);
//            System.out.println(data.getString().toString());
            transferFromOPN();
        } catch (Exception e) {
            return null;
        }
        return data.getAndRemoveResultStack();
    }

    private static void transferToOPN(String sequence) throws Exception {
        char[] chars = sequence.toCharArray();
        for(int i=0; i < chars.length; i++){
            if(isDot(String.valueOf(chars[i]))){
                i = findNumberByDotAndReturnLastIndex(chars, i);
                continue;
            }
            if(isNumber(String.valueOf(chars[i]))){
                findPrevNumber(chars, i);
            } else {
                classification(chars[i]);
            }
        }
        while (!data.isEmptyStack()){
            data.addString(data.getAndRemoveStack());
        }
    }

    private static void transferFromOPN() throws Exception {
        for(String symbol : data.getString()){
            if(isDoubleNumber(symbol)){
                data.addResultStack(symbol);
            } else {
                calculation(symbol);
            }
        }
    }

    private static boolean isDot(String symbol){
        Pattern pattern = Pattern.compile(DOT);
        Matcher matcher = pattern.matcher(symbol);
        return matcher.matches();
    }

    private static boolean isNumber(String symbol){
        Pattern pattern = Pattern.compile(SYMBOL_PATTERN);
        Matcher matcher = pattern.matcher(symbol);
        return matcher.matches();
    }

    private static boolean isDoubleNumber(String symbol){
        Pattern patternDouble = Pattern.compile(DOUBLE_NUMBER_PATTERN);
        Matcher matcherDouble = patternDouble.matcher(symbol);
        Pattern patternNumber = Pattern.compile(NUMBER_PATTERN);
        Matcher matcherNumber = patternNumber.matcher(symbol);
        return matcherDouble.matches() || matcherNumber.matches();
    }

    private static int findPrevNumber(char[] chars, int index){
        List<String> dataString = data.getString();
        int currentIndex = index;
        String resultNumber = new String();
        if(--currentIndex >= 0 && isNumber(String.valueOf(chars[currentIndex]))){
            resultNumber = dataString.remove(dataString.size()-1) + resultNumber;
        }
        resultNumber = resultNumber + chars[index];
        dataString.add(resultNumber);
        data.setString(dataString);
        return index;
    }

    private static int findNumberByDotAndReturnLastIndex(char[] chars, int index) throws Exception {
        List<String> dataString = data.getString();
        int errorIndexLength = 0;
        int currentIndex = index;
        String resultNumber = new String();
        if(--currentIndex >= 0 && isNumber(String.valueOf(chars[currentIndex]))){
            resultNumber = dataString.remove(dataString.size()-1) + resultNumber;
            errorIndexLength++;
        }

        if(errorIndexLength == 0){
            throw new Exception();
        }
        currentIndex = index;
        errorIndexLength = 0;
        resultNumber = resultNumber + ".";
        while(++currentIndex!=chars.length && isNumber(String.valueOf(chars[currentIndex]))){
            resultNumber = resultNumber + chars[currentIndex];
            errorIndexLength++;
        }
        if(errorIndexLength == 0){
            throw new Exception();
        }
        dataString.add(resultNumber);
        data.setString(dataString);
        return --currentIndex;
    }

    private static void classification(char symbol) throws Exception {
        String str = String.valueOf(symbol);
        switch (OPERATION.getBySymbol(str)) {
            case PLUS:
            case MINUS:
            case MULTIPLY:
            case DIVIDE:
                checkPriority(str);
                break;
            case SKOBKA_OPEN:
                data.addStack(str);
                break;
            case SKOBKA_CLOSE:
                ckobkaClosedFatality();
                break;
            default:
                throw new Exception();
        }
    }

    private static void calculation(String symbol) throws Exception {
        Pair<BigDecimal, BigDecimal> operands;
        switch (OPERATION.getBySymbol(symbol)) {
            case PLUS:
                operands = initOperands();
                data.addResultStack(operands.getKey().add(operands.getValue()).toString());
                break;
            case MINUS:
                operands = initOperands();
                data.addResultStack(operands.getValue().subtract(operands.getKey()).toString());
                break;
            case MULTIPLY:
                operands = initOperands();
                data.addResultStack(operands.getKey().multiply(operands.getValue()).toString());
                break;
            case DIVIDE:
                operands = initOperands();
                data.addResultStack(operands.getValue().divide(operands.getKey(), 4, BigDecimal.ROUND_HALF_UP).toString());
                break;
            default:
                throw new Exception();
        }
    }

    private static Pair<BigDecimal, BigDecimal> initOperands(){
        BigDecimal operand1 = BigDecimal.valueOf(Double.valueOf(data.getAndRemoveResultStack()));
        BigDecimal operand2 = BigDecimal.valueOf(Double.valueOf(data.getAndRemoveResultStack()));
        operand1 = operand1.setScale(4, BigDecimal.ROUND_HALF_UP);
        operand2 = operand2.setScale(4, BigDecimal.ROUND_HALF_UP);
        return new Pair<>(operand1, operand2);
    }

    private static void checkPriority(String symbol){
        if(OPERATION.getBySymbol(data.getStack()).getPriority() < OPERATION.getBySymbol(symbol).getPriority()){
            data.addStack(symbol);
        } else {
            while (OPERATION.getBySymbol(data.getStack()).getPriority() >= OPERATION.getBySymbol(symbol).getPriority()){
                String removed = data.getAndRemoveStack();
                data.addString(removed);
            }
            data.addStack(symbol);
        }
    }

    private static void ckobkaClosedFatality() throws Exception {
        if(!data.containsSymbol(OPERATION.SKOBKA_OPEN.getSymbol())){
            throw new Exception();
        }
        while (!data.isEmptyStack() &&
                !OPERATION.SKOBKA_OPEN.getSymbol().equals(OPERATION.getBySymbol(data.getStack()).getSymbol())){
            String removed = data.getAndRemoveStack();
            data.addString(removed);
        }
        data.getAndRemoveStack();
    }

    static class OPNData{
        private List<String> string = new ArrayList<>();
        private Deque<String> stack = new ArrayDeque<>(25);
        private Deque<String> resultStack = new ArrayDeque<>(25);

        OPNData(){}

        public void setString(List<String> strs){
            this.string = strs;
        }

        public List<String> getString() {
            return string;
        }

        public String getAndRemoveStack() {
            return stack.poll();
        }

        public String getStack() {
            return stack.peek();
        }

        public void addString(String str){
            this.string.add(str);
        }

        public void addStack(String stack){
            this.stack.push(stack);
        }

        public boolean isEmptyStack(){
            return stack.isEmpty();
        }

        public void addResultStack(String str){
            resultStack.push(str);
        }

        public String getAndRemoveResultStack(){
            return resultStack.poll();
        }

        public boolean containsSymbol(String symbol){
            return stack.contains(symbol);
        }
    }

    enum OPERATION{
        UNDEFINED("",-1),
        PLUS("+",2),
        MINUS("-",2),
        SKOBKA_OPEN("(",1),
        SKOBKA_CLOSE(")",1),
        DIVIDE("/",3),
        MULTIPLY("*",3);

        private OPERATION(String symbol, Integer priority){
            this.symbol = symbol;
            this.priority = priority;
        }
        private Integer priority;
        private String symbol;

        public static OPERATION getByPriority(Integer priority){
            return Arrays.stream(values())
                    .filter(operation -> operation.getPriority().equals(priority))
                    .findAny()
                    .orElse(UNDEFINED);
        }

        public static OPERATION getBySymbol(String symbol){
            return Arrays.stream(values())
                    .filter(operation -> operation.getSymbol().equals(symbol))
                    .findAny()
                    .orElse(UNDEFINED);
        }

        public Integer getPriority() {
            return priority;
        }

        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }
    }
}
