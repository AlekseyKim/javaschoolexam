package pp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Алексей on 10.03.2017.
 */
public class FileReader {
    private static final String PATH_READ = "D:\\Java\\task001\\src\\pp\\input.txt";
    private static final String PATH_WRITE = "D:\\Java\\task001\\src\\pp\\output.txt";

    public void readFile(){
        try {
            Stream<String> stream = Files.lines(Paths.get(PATH_READ), StandardCharsets.UTF_8);
            Map<String, Long> result = stream.collect(
                    Collectors.groupingBy(
                            Function.identity(), Collectors.counting()));

            List<String> result1 = result.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .map(entry -> entry.getKey() + "[" + entry.getValue() + "]")
                    .collect(Collectors.toList());
            Files.write(Paths.get(PATH_WRITE), result1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
